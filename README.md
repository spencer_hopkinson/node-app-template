# README #

A template framework for an API written in NodeJS using Express

### What is this repository for? ###

* A framework for creating a RESTful API in NodeJS using express.

### How do I get set up? ###

* Clone the repo
* Run "npm install" to install dependencies
* To run the api: run "npm start" OR ./docker-run.sh to run in a container
* To run the api with nodemon watching for file changes run "npm run start:dev"
* To run the unit tests, run "npm run test"
* To run the integration tests, ensure the api is running (as above) then run "npm run test:int"
* To run the coverage report, run "npm run coverage"
* For linting, run "npm run lint"
* For lint fixing, run "npm run lint:fix"
* To run the test runner in watch mode (running on tests changing), run "npm run test:watch"
* Commits will invoke the pre-commit Git hook which will enforce linting and unit test validation