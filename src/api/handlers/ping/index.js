const express = require('express');

const router = express.Router();
const getPing = require('./getPing');

// middleware point
router.use((req, res, next) => {
  console.log(`PING ROUTER MIDDLEWARE: ${Date.now()}`); // eslint-disable-line no-console
  next();
});

router.get('/', (req, res) => {
  getPing(req, res);
});

module.exports = router;
