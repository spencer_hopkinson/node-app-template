// const fs = require('fs');

// console.log('loading environment config...');
// console.log('environment argument: %s', process.env.NODE_ENV);

const env = process.env.NODE_ENV || 'dev';
const configfile = `./config.${env}.js`;

// fs.exists(configfile,function(exists){
//   console.log(exists);
//   if (exists)
//     throw new Error('Config file:' + configfile + ' does not exist!');
// });

module.exports = require(configfile); // eslint-disable-line import/no-dynamic-require

// console.log('Configuration:\t%s', JSON.stringify(module.exports));
