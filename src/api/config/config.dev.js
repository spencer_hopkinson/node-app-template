module.exports = {
  debug: true,
  logging: {
    httplog: 'logs/httplog.txt',
    errorLog: 'logs/errorLog.txt',
    log(message) {
      console.log(message); // eslint-disable-line no-console
    },
  },
  database: {
    connectionString: 'mongodb://127.0.0.1:27017/api',
    databaseName: 'api',
  },
};
