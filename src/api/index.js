/*
ENTRY POINT FOR THE API SYSTEM
*/

// var config = require('./Config-debug');
// var winston = require('winston');
// var mongoose = require('mongoose');
const server = require('./server');

// We will log normal api operations into api.log
// console.log("starting logger...");
// winston.add(winston.transports.File, {
//  filename: config.logger.api
// });

// We will log all uncaught exceptions into exceptions.log
// winston.handleExceptions(new winston.transports.File({
//  filename: config.logger.exception
// }));

// console.log("logger started. Connecting to MongoDB...");
// mongoose.connect(config.db.mongodb);

server.start();
