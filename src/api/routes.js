/*
DEFINE API ROUTES AND HANDLERS
*/
const pingHandler = require('./handlers/ping');

module.exports = {
  setup(app) {
    // Mount ping API
    app.use('/api/ping', pingHandler);

    // Add routes here:
  },
};
