const bodyParser = require('body-parser');
const express = require('express');

const app = express();
const errorHandler = require('errorhandler');
const config = require('./config');
const routes = require('./routes');
const dataAccess = require('./dataAccess/mongo');

app.use(bodyParser.json());

if (app.settings.env === 'development') {
  app.use(errorHandler({ log: true }));
}

if (app.settings.env === 'production') {
  app.use(errorHandler({ log: false }));
}

app.use((req, res, next) => {
  // Inject your logging here
  const logModel = {
    method: req.method,
    body: req.body,
    headers: req.headers,
    dateTime: new Date(),
  };

  console.log(`MIDDLEWARE LOGGER: ${JSON.stringify(logModel)}`); // eslint-disable-line no-console
  dataAccess.add('HttpLog', logModel, (err) => {
    if (err) {
      console.log(err); // eslint-disable-line no-console
    }
  });
  next();
});

module.exports = {
  start() {
    config.logging.log('setting up routes');
    routes.setup(app);
    config.logging.log('routes defined.');
    const port = process.env.PORT || 8081;
    app.listen(port);
    // http://expressjs.com/en/api.html#app.settings.table
    config.logging.log(
      `Server listening on port ${port} in ${app.settings.env} mode`,
    );
  },
};
