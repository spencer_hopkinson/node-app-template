const client = require('mongodb').MongoClient;
const config = require('../config');

module.exports = {
  async add(collectionName, model, callback) {
    client.connect(config.database.connectionString, async (err, clientInstance) => {
      if (err) {
        callback(err);
        return;
      }
      const db = clientInstance.db(config.database.databaseName);
      try {
        await db.collection(collectionName).insertOne(model);
      } catch (insertError) {
        throw new Error(insertError);
      }
    });
  },
};
