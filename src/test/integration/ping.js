const assert = require('assert');
const request = require('supertest');

describe('Ping', () => {
  const url = 'http://localhost:8081';
  describe('Ping Route', () => {
    it('should return 200', (done) => {
      request(url)
        .get('/api/ping')
        // end handles the response
        .end((err, res) => {
          if (err) {
            throw err;
          }
          assert.equal(res.status, 200);
          done();
        });
    });

    it('should return version 1.0.0 in the body', (done) => {
      request(url)
        .get('/api/ping')
        .expect({ version: '1.0.0' }, done);
    });
  });
});
