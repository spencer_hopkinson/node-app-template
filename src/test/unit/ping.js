const assert = require('assert');
const getPing = require('../../api/handlers/ping/getPing');

const res = {
  json(arg) {
    this.json = arg;
  },
};

describe('Ping', () => {
  it('Get returns version 1.0.0', () => {
    getPing({}, res);
    assert(res.json.version === '1.0.0');
  });
});
