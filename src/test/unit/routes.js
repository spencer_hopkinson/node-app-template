const sinon = require('sinon');
const assert = require('assert');
const routes = require('../../api/routes');

describe('Routes', () => {
  describe('Setup', () => {
    it('should call app.use once', () => {
      const app = {};
      const spy = sinon.spy();

      app.use = spy;
      routes.setup(app);

      assert.equal(true, spy.calledOnce); // 1 route configured - ping.get
    });
  });
});
