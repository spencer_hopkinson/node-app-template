FROM node:latest
ARG appName=app
RUN echo $appName
# RUN mkdir -p /usr/src/api
WORKDIR /usr/src/api
RUN mkdir logs
COPY package*.json ./
RUN npm install --production
COPY . .
RUN ls
CMD ["npm","start"]
