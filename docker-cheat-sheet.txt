Elevate shell to root for docker commands (means you don't have to do: sudo docker ...)
	sudo bash

List images:

	docker image ls

List containers:
	
	docker container ls

Shell into a running container:
	
	docker exec -it <container id> /bin/bash


