set appName=node_app
REM The -d switch runs in detached mode, this will start the container in the background then output the container id
docker run -p 8081:8081 -d spencer/%appName%
