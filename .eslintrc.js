module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
    mocha:true
  },
  extends: 'airbnb-base',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    strict:0,
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'no-unused-vars': ["error", { "args": "none" }]
  },
};
